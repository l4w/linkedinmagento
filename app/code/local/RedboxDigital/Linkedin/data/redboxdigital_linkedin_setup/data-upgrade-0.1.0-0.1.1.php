<?php
/**
 * Light4website
 *
 * @copyright  Copyright(c) 2015 light4website (http://light4website.com)
 * @license    http://light4website.com/license/license.txt
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = Mage::getResourceModel('customer/setup', 'customer_setup');

$installer->startSetup();

$entityTypeId = Mage::getModel('customer/customer')->getResource()->getTypeId();
$customerEntity = 'customer';

$attributeSetName = 'Default';
$attributeSetData = $installer->getAttributeSet($entityTypeId, $attributeSetName);
$group = 'General';

$textAttribute = array(
    'attribute_code' => 'linkedin_profile',
    'attribute_label' => 'Linkedin Profile'
);

if (isset($attributeSetData['attribute_set_id'])) {
        $installer->addAttribute($customerEntity, $textAttribute['attribute_code'], array(
            'attribute_set_id'  => $attributeSetData['attribute_set_id'],
            'label'             => $textAttribute['attribute_label'],
            'type'              => 'varchar',
            'input'             => 'text',
            'backend'           => '',
            'source'            => '',
            'frontend'          => '',
            'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
            'visible'           => true,
            'required'          => true,
            'user_defined'      => true,
            'unique'            => false,
            'validate_rules'    => serialize(array(
                'input_validation' => 'url',
                'max_text_length'   => 250,
                'min_text_length'   => 1
            )),
            'position'          => 121,
        ));

        $installer->addAttributeGroup($entityTypeId, $attributeSetData['attribute_set_id'], $group);
        $installer->addAttributeToSet($entityTypeId, $attributeSetData['attribute_set_id'], $group, $textAttribute['attribute_code'], null);

        $usedInForms = array(
            'customer_account_create',
            'customer_account_edit',
            'checkout_register',
            'adminhtml_customer'
        );

        $attribute = Mage::getSingleton('eav/config')->getAttribute($entityTypeId, $textAttribute['attribute_code']);
        $attribute->setData('used_in_forms', $usedInForms);
        $attribute->setData('is_system', 0);
        $attribute->save();
}

$installer->endSetup();