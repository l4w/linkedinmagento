<?php
/**
 * Light4website
 *
 * @copyright  Copyright(c) 2014 light4website (http://light4website.com)
 * @license    http://light4website.com/license/license.txt
 */
/* @var $installer Mage_Sales_Model_Resource_Setup */
$installer = Mage::getResourceModel('sales/setup', 'sales_setup');
$installer->startSetup();

$entitiesToAlter = array('quote','order');
$attributes = array(
    'customer_linkedin_profile' => array(
        'type'      => 'varchar',
        'nullable'  => true,
        'default'   => null,
        'comment'   => 'Linkedin Profile'
    )
);

foreach ($entitiesToAlter as $entityName) {
    foreach ($attributes as $attributeCode => $attributeParams) {
        $installer->addAttribute($entityName, $attributeCode, $attributeParams);
    }
}

$installer->endSetup();