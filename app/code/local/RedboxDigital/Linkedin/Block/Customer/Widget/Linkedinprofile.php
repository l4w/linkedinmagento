<?php
/**
 * Light4website
 *
 * @copyright  Copyright(c) 2015 light4website (http://light4website.com)
 * @license    http://light4website.com/license/license.txt
 */

class RedboxDigital_Linkedin_Block_Customer_Widget_Linkedinprofile extends Mage_Customer_Block_Widget_Abstract
{
    const ATTRIBUTE_CODE = 'linkedin_profile';

    public function _construct()
    {
        $this->_isRequired = false;
        parent::_construct();
        $this->setTemplate('redboxdigital/linkedin/customer/widget/linkedinprofile.phtml');
    }

    public function isRequired()
    {
        return (bool)$this->_getAttribute(self::ATTRIBUTE_CODE)->getIsRequired();
    }

    /**
     * Retrieve value
     *
     * @return bool|string
     */
    public function getValue()
    {
        if ($linkedinProfile = $this->getLinkedinProfile()) {
            return $this->escapeHtml($linkedinProfile);
        }
        return false;
    }

    /**
     * Retrieve validation class
     *
     * @return string
     */
    public function getAttributeValidationClass()
    {
        return $this->_helper()->getAttributeValidationClass(self::ATTRIBUTE_CODE);
    }

    /**
     * Get module's main helper
     *
     * @return RedboxDigital_Linkedin_Helper_Data
     */
    protected function _helper()
    {
        return Mage::helper('redboxdigital_linkedin');
    }
}