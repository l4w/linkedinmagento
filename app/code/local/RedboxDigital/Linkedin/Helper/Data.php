<?php
/**
 * Light4website
 *
 * @copyright  Copyright(c) 2015 light4website (http://light4website.com)
 * @license    http://light4website.com/license/license.txt
 */ 
class RedboxDigital_Linkedin_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getAttributeValidationClass($attributeCode)
    {
        $class = '';

        /** @var $customerAttribute Mage_Customer_Model_Attribute */
        $customerAttribute = Mage::getSingleton('eav/config')->getAttribute('customer', $attributeCode);
        $class .= $customerAttribute && $customerAttribute->getIsVisible()
            ? $customerAttribute->getFrontend()->getClass() : '';
        $class = implode(' ', array_unique(array_filter(explode(' ', $class))));

        return $class;
    }
}