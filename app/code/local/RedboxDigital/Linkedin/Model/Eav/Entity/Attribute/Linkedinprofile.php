<?php
/**
 * Light4website
 *
 * @copyright  Copyright(c) 2015 light4website (http://light4website.com)
 * @license    http://light4website.com/license/license.txt
 */

class RedboxDigital_Linkedin_Model_Eav_Entity_Attribute_Linkedinprofile extends Mage_Core_Model_Config_Data
{
    /**
     * After change Linkedin Profile attribute value process
     *
     * @return RedboxDigital_Linkedin_Model_Eav_Entity_Attribute_Linkedinprofile
     */
    protected function _afterSave()
    {
        $attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'linkedin_profile');
        $value  = $this->getValue();
        switch ($this->getScope()) {
            case 'websites':
                $website = Mage::app()->getWebsite($this->getWebsiteCode());
                $attribute->setWebsite($website);
                $attribute->load($attribute->getId());
                if ($attribute->getData('is_required') != $value) {
                    $attribute->setData('is_required', (bool)$value);
                }
                break;

            case 'default':
                $attribute->setData('is_required', (bool)$value);
                break;
        }
        $attribute->save();
        return $this;
    }
}